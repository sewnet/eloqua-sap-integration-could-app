from celery.utils.log import get_task_logger
from datetime import datetime, timedelta
from celery.utils.log import get_task_logger
from dea import EloquaClient
from dea.bulk.definitions import ImportDefinition
from dea.bulk.eml import eml
from dea.eloqua import DEFAULT_RETRY
from flask import current_app
from requests.adapters import HTTPAdapter

from ruukki_db_loader_feeder.db import db_crud
from . import celery
from .database import get_db
from .session import get_eloqua_session
from .eloqua_inbound import inbound_config

logger = get_task_logger(__name__)


def _get_eloqua_client(install_id, app_id):
    oauth = get_eloqua_session(install_id)
    adapter = HTTPAdapter(max_retries=DEFAULT_RETRY)
    oauth.mount("http://", adapter)
    oauth.mount("https://", adapter)

    return EloquaClient(base_url=get_db().get_base_url_for(app_id, install_id),
                        auto_identify=False, session=oauth)


@celery.task
def elq_contact_import(data, fields, install_id, app_id, instance_id, task_type, sync=None):
    client = EloquaClient(base_url=get_db().get_base_url_for(app_id, install_id), auto_identify=False,
                          session=get_eloqua_session(install_id))
    if sync is None:
        sync_action = {
            "action": "setStatus",
            "destination": eml.FeederInstance(instance_id),
            "status": "complete"
        }
    else:
        sync_action = sync
    contact_import_def = ImportDefinition(name="Definition for {}".format(task_type), fields=fields,
                                          id_field_name="C_EmailAddress",
                                          sync_actions=sync_action)
    with client.bulk_contacts.imports.create_import(import_def=contact_import_def) as bulk_import:
        bulk_import.add_items(data)
        bulk_import.upload_and_flush_data()
        bulk_import.sync_uploaded_data()
    logger.debug("Contact import complete.")
    logger.debug("Done!")


@celery.task
def elq_account_import(data, fields, install_id, app_id, instance_id, task_type):
    client = _get_eloqua_client(install_id=install_id, app_id=app_id)

    account_import_def = ImportDefinition(name="Definition for {}".format(task_type), fields=fields,
                                          id_field_name="M_SFDCAccountID")
    with client.bulk_accounts.imports.create_import(import_def=account_import_def) as bulk_import:
        bulk_import.add_items(data)
        bulk_import.upload_and_flush_data()
        bulk_import.sync_uploaded_data()
    logger.debug("Account import complete.")
    logger.debug("Done!")


@celery.task
def hourly_tasks():
    DATE_TIME_NOW = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    DATE_TIME_HOURLY = (datetime.now() - timedelta(hours=5)).strftime("%Y-%m-%d %H:%M:%S")
    active_instances = get_db().get_hourly_active_instances()
    for instance in active_instances:
        task = instance["configuration"]["custom"]["task"]
        fields = instance["configuration"]["custom"]["fields"]
        install_id = instance["install_id"]
        app_id = instance["app_id"]
        instance_id = instance["instance_id"]
        with db_crud.start_psql_session() as session:
            data1 = db_crud.inbound_data(session_=session, task=task,
                                         date_from=DATE_TIME_HOURLY,
                                         date_to=DATE_TIME_NOW)
            logger.debug("Importing {} {} ############################".format(len(data1), task))
            if task == "Get_Contacts":
                elq_contact_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            elif task == "Get_Accounts":
                elq_account_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            elif task == "Get_International_Leads":
                elq_contact_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            elif task == "Get_Employees_to_Contact":
                elq_contact_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            elif task == "Get_Finnish_Leads":
                elq_contact_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            else:
                logger.debug("Nothing To import for hourly frequency")


@celery.task
def daily_tasks():
    DATE_TIME_NOW = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    DATE_TIME_DAILY = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d %H:%M:%S")
    active_instances = get_db().get_daily_active_instances()
    for instance in active_instances:
        task = instance["configuration"]["custom"]["task"]
        fields = instance["configuration"]["custom"]["fields"]
        install_id = instance["install_id"]
        app_id = instance["app_id"]
        instance_id = instance["instance_id"]
        add_to_shared_list = "Add_to_shared_list"
        add_to_shared_list_field = inbound_config.inbound_task_fields["Add_to_shared_list"]
        with db_crud.start_psql_session() as session:
            data1 = db_crud.inbound_data(session_=session, task=add_to_shared_list,
                                         date_from=DATE_TIME_DAILY,
                                         date_to=DATE_TIME_NOW)
            logger.debug("Importing {} {} ############################".format(len(data1), add_to_shared_list))
            shared_lists = {}
            for contact in data1:
                if contact["targer_group_id"] not in shared_lists:
                    shared_lists[contact["targer_group_id"]] = []
                shared_lists[contact["targer_group_id"]].append({"C_EmailAddress": contact["C_EmailAddress"]})
            for list_id, item in shared_lists.items():
                sync_action = {
                    "action": "add",
                    "destination": eml.ContactList(list_id)
                }
                logger.debug("##############{} conatcts for List ID {} ".format(len(item),list_id))
                elq_contact_import(data=item, fields=add_to_shared_list_field, install_id=install_id,
                                   instance_id=instance_id,
                                   task_type=add_to_shared_list, app_id=app_id, sync=sync_action)
        with db_crud.start_psql_session() as session:
            data1 = db_crud.inbound_data(session_=session, task=task,
                                         date_from=DATE_TIME_DAILY,
                                         date_to=DATE_TIME_NOW)
            logger.debug("Importing {} {} ############################".format(len(data1), task))
            if task == "Get_Contacts":
                elq_contact_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            elif task == "Get_Accounts":
                elq_account_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            elif task == "Get_International_Leads":
                elq_contact_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            elif task == "Get_Employees_to_Contact":
                elq_contact_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            elif task == "Get_Finnish_Leads":
                elq_contact_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            else:
                logger.debug("Nothing To import for hourly frequency")


@celery.task
def weekly_tasks():
    DATE_TIME_NOW = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    DATE_TIME_WEEKLY = (datetime.now() - timedelta(weeks=1)).strftime("%Y-%m-%d %H:%M:%S")
    active_instances = get_db().get_weekly_active_instances()
    for instance in active_instances:
        task = instance["configuration"]["custom"]["task"]
        with db_crud.start_psql_session() as session:
            data1 = db_crud.inbound_data(session_=session, task=task,
                                         date_from=DATE_TIME_WEEKLY,
                                         date_to=DATE_TIME_NOW)
            logger.debug("Importing {} {} ############################".format(len(data1), task))
            fields = instance["configuration"]["custom"]["fields"]
            install_id = instance["install_id"]
            app_id = instance["app_id"]
            instance_id = instance["instance_id"]
            if task == "Get_Contacts":
                elq_contact_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            elif task == "Get_Accounts":
                elq_account_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            elif task == "Get_International_Leads":
                elq_contact_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            elif task == "Get_Employees_to_Contact":
                elq_contact_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            elif task == "Get_Finnish_Leads":
                elq_contact_import(data=data1, fields=fields, install_id=install_id, instance_id=instance_id,
                                   task_type=task, app_id=app_id)
            else:
                logger.debug("Nothing To import for hourly frequency")
