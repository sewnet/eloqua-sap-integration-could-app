import logging

from dea import EloquaClient
from dea.bulk.definitions import SyncActionsDefinition
from dea.bulk.eml import eml
from flask import Blueprint, request, render_template, flash
from typing import List, Dict, Tuple

from ruukki_db_loader_feeder.db import db_crud
from .common import DefaultCreateView, DefaultDeleteView, BaseConfigureView, BaseAsyncNotifyView
from ..database import get_db
from ..eloqua_inbound import outbound_config
from ..integration_tasks import inbound_tasks
from ..session import get_eloqua_session
from ..util import ServiceRequest

logger = logging.getLogger(__name__)


class NotifyView(BaseAsyncNotifyView):
    default_name = "sap_integration_notify"

    def process_items(self, app_id: str, install_id: str, instance_id: str, items: List[Dict], total_results,
                      execution_id: str = None) -> None:
        args = ServiceRequest(request)
        args.instance_id = instance_id
        db = get_db()
        db.insert_service_log(self.default_name, {"total_results": total_results, "instance_id": instance_id,
                                                  "install_id": install_id, "app_id": app_id, **args.as_dict()})
        logger.debug("Received %s item(s).", total_results)
        db.insert_execution(app_id, install_id, instance_id, execution_id)
        # process contact here
        success_contcts = []
        error_contacts = []
        for lead in items:
            contact_id = lead["id"]
            data_to_sync = {"id": contact_id}
            insert_lead = self.db_insert_leads(execution_id=execution_id, lead=lead)
            if insert_lead:
                success_contcts.append(data_to_sync)
            else:
                error_contacts.append(data_to_sync)
        if len(success_contcts) > 0:
            logger.debug("Syncing {} Success contacts".format(len(success_contcts)))
            self.import_contact_status(success=True, contacts=success_contcts, execution_id=execution_id)
        elif len(error_contacts) > 0:
            logger.debug("Syncing {} Errored contacts".format(len(error_contacts)))
            self.import_contact_status(success=False, contacts=error_contacts, execution_id=execution_id)

    @staticmethod
    def db_insert_leads(execution_id, lead):
        execution_info = get_db().get_execution(execution_id)
        instance_config = execution_info["instance_config"]
        with db_crud.start_psql_session() as session:
            db_crud.outbound_data(sessions_=session, data=lead, task=instance_config["task"])
        logger.debug("Lead inserted to DB ######################")

        return True

    @staticmethod
    def import_contact_status(success, execution_id, contacts):
        execution_info = get_db().get_execution(execution_id)
        app_id = execution_info["app_id"]
        install_id = execution_info["install_id"]
        instance_id = execution_info["instance_id"]

        logger.debug(f"Importing contact status to Eloqua...")
        import_def = SyncActionsDefinition(
            f"Lead DB import #{execution_id} for contacts",
            fields={
                "id": eml.Contact.Id,
            },
            id_field_name="id",
            sync_actions={
                "action": "setStatus",
                "destination": eml.ActionInstance(instance_id).Execution(execution_id),
                "status": "complete" if success else "errored"
            })

        client = EloquaClient(session=get_eloqua_session(install_id))
        with client.bulk_contacts.imports.create_sync_action(import_def) as bulk_import:
            bulk_import.add_items(contacts)
            bulk_import.upload_and_flush_data(sync_on_upload=True)
        logger.debug("Import complete.")
        logger.debug("Done!")


class ConfigureView(BaseConfigureView):
    default_name = "sap_integration_config"

    def validate_form(self) -> Tuple[bool, List[str]]:
        messages = []
        form = request.form
        selected_task = form.get("task", None)

        if selected_task == "None":
            messages.append("Please select a task")

        return len(messages) == 0, messages

    def get(self, app_id, install_id, instance_id, *args, **kwargs) -> Tuple[str, int]:
        return self.default_response()

    def post(self, app_id, install_id, instance_id, *args, **kwargs) -> Tuple[str, int]:
        is_valid, errors = self.validate_form()

        if not is_valid:
            for error in errors:
                flash(error, "error")
            return self.default_response(request.form)

        # Get configurations from the database.
        eloqua_config = self.get_eloqua_config()
        new_eloqua_config = {
            "recordDefinition": {**outbound_config.outbound_task_fields["crud_leads"]},
            "requiresConfiguration": False
        }

        # Save Eloqua side configurations first (if needed).
        if new_eloqua_config != eloqua_config:
            logger.debug("Changes detected in Eloqua configuration.")
            logger.debug(f"Old: {eloqua_config}")
            logger.debug(f"New: {new_eloqua_config}")
            client = EloquaClient(session=get_eloqua_session(install_id))
            client.put(client.url_for("/api/cloud/1.0/actions/instances/{instance_id}", instance_id=instance_id),
                       json=new_eloqua_config)
            self.save_eloqua_config(new_eloqua_config)
            logger.debug("Eloqua configuration updated.")
        self.save_config({
            "task": request.form["task"]
        })

        flash("Changes saved.", "success")
        return self.default_response()

    def default_response(self, form_values: dict = None) -> Tuple[str, int]:
        logger.debug(f"config: {self.get_config()}")
        form_values = self.get_config() or form_values or {}
        return render_template("config.html", subtitle="Instance configuration", tasks=inbound_tasks,
                               form_values=form_values)


bp = Blueprint("eloqua_sap_integration", __name__, url_prefix="/eloqua/sap-integration")
DefaultCreateView.add_url_rule_to(bp, "sap_integration_create",
                                  record_definition=outbound_config.outbound_task_fields["crud_leads"],
                                  requires_configuration=True,
                                  instance_config={})
DefaultDeleteView.add_url_rule_to(bp, "sap_integration_delete")
NotifyView.add_url_rule_to(bp)
ConfigureView.add_url_rule_to(bp)
