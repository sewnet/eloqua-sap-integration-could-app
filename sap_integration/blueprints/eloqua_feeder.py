import logging
from typing import List, Dict, Tuple

from dea.bulk.eml import eml
from flask import Blueprint, request, render_template, flash
import sap_integration.tasks as tasks
from .common import DefaultCreateView, DefaultDeleteView, BaseConfigureView, BaseAsyncNotifyView
from ..database import get_db
from ..eloqua_inbound import inbound_config
from ..integration_tasks import outbound_tasks, frequencies
from ..session import get_eloqua_session
from ..util import ServiceRequest
from flask import current_app
from celery.schedules import crontab
from celery import Celery
from .. import celery
from ..settings import *

logger = logging.getLogger(__name__)
app = Celery()

DEFAULT_RECORD_DEFINITION = {
    "id": eml.Contact.Id,
    "company_name": eml.Contact.Field("C_Company"),
    "email_address": eml.Contact.Field("C_EmailAddress"),
    "country": eml.Contact.Field("C_Country"),
    "first_name": eml.Contact.Field("C_FirstName")
}


class NotifyView(BaseAsyncNotifyView, BaseConfigureView):
    default_name = "sap_integration_notify"

    def process_items(self, app_id: str, install_id: str, instance_id: str, items: List[Dict], total_results,
                      execution_id: str = None) -> None:
        args = ServiceRequest(request)
        args.instance_id = instance_id
        db = get_db()
        db.insert_service_log(self.default_name, {"total_results": total_results, "instance_id": instance_id,
                                                  "install_id": install_id, "app_id": app_id, **args.as_dict()})
        db.insert_execution(app_id, install_id, instance_id, execution_id)
        self.save_config(
            {
                "status": args.event_type
            },
            instance_id=instance_id,
            app_id=app_id,
            install_id=install_id,
            partial_update=True)
        # process contact here


class ConfigureView(BaseConfigureView):
    default_name = "sap_feeder_config"

    def validate_form(self) -> Tuple[bool, List[str]]:
        messages = []
        form = request.form
        selected_task = form.get("task", None)

        if selected_task == "None":
            messages.append("Please select a task")

        return len(messages) == 0, messages

    def get(self, app_id, install_id, instance_id, *args, **kwargs) -> Tuple[str, int]:
        return self.default_response()

    def post(self, app_id, install_id, instance_id, *args, **kwargs) -> Tuple[str, int]:
        is_valid, errors = self.validate_form()

        if not is_valid:
            for error in errors:
                flash(error, "error")
            return self.default_response(request.form)

        # Get configurations from the database.
        eloqua_config = self.get_eloqua_config()
        new_eloqua_config = {
            "requiresConfiguration": False
        }

        # Save Eloqua side configurations first (if needed).
        if new_eloqua_config != eloqua_config:
            logger.debug("Changes detected in Eloqua configuration.")
            logger.debug(f"Old: {eloqua_config}")
            logger.debug(f"New: {new_eloqua_config}")
            from dea import EloquaClient
            client = EloquaClient(session=get_eloqua_session(install_id))
            client.put(client.url_for("/api/cloud/1.0/feeders/instances/{instance_id}", instance_id=instance_id),
                       json=new_eloqua_config)
            self.save_eloqua_config(new_eloqua_config)
            logger.debug("Eloqua configuration updated.")
        self.save_config({
            "task": request.form["task"],
            "frequency": request.form["frequency"],
            "fields": inbound_config.inbound_task_fields[request.form["task"]]
        })

        flash("Changes saved.", "success")
        return self.default_response()

    def default_response(self, form_values: dict = None) -> Tuple[str, int]:
        logger.debug(f"config: {self.get_config()}")
        form_values = self.get_config() or form_values or {}
        return render_template("feeder_config.html", subtitle="Instance configuration", tasks=outbound_tasks,
                               frequencies=frequencies, form_values=form_values)


bp = Blueprint("eloqua_sap_feeder", __name__, url_prefix="/eloqua/sap-feeder")
DefaultCreateView.add_url_rule_to(bp, "sap_feeder_create", record_definition=DEFAULT_RECORD_DEFINITION,
                                  requires_configuration=True,
                                  instance_config={})
DefaultDeleteView.add_url_rule_to(bp, "sap_feeder_delete")
NotifyView.add_url_rule_to(bp)
ConfigureView.add_url_rule_to(bp)
