import logging.config

import yaml
from celery import Celery
from flask import Flask

import sap_integration.settings

logger = logging.getLogger(__name__)
celery = Celery()


def create_app():
    """
    Application factory for creating Flask apps.
    """
    with open(sap_integration.settings.LOGGING_CONFIG, "r") as f:
        global logger
        logging.config.dictConfig(yaml.safe_load(f))
        logger = logging.getLogger(__name__)
        logger.debug("Logging configured.")

    app = Flask(__name__)
    app.config.from_object("sap_integration.settings")
    app.logger.debug("Configured Flask app.")

    register_blueprints(app)
    return app


def register_blueprints(app):
    """
    Registers all the blueprints for the Flask application.
    """
    # Cloud app lifecycle endpoints.
    import sap_integration.blueprints.lifecycle
    app.register_blueprint(sap_integration.blueprints.lifecycle.bp)

    # Cloud app OAuth endpoints.
    import sap_integration.blueprints.oauth
    app.register_blueprint(sap_integration.blueprints.oauth.bp)

    # action  step endpoints.
    import sap_integration.blueprints.sap_integration
    app.register_blueprint(sap_integration.blueprints.sap_integration.bp)

    # feeder step endpoints.
    import sap_integration.blueprints.eloqua_feeder
    app.register_blueprint(sap_integration.blueprints.eloqua_feeder.bp)

    app.logger.debug("Registered blueprints.")


def init_celery(app=None):
    app = app or create_app()
    celery.conf.broker_url = app.config["CELERY_BROKER_URL"]
    celery.conf.result_backend = app.config["CELERY_RESULT_BACKEND"]
    celery.conf.imports = app.config["CELERY_IMPORTS"]
    celery.conf.task_routes = app.config["CELERY_TASK_ROUTES"]
    celery.conf.beat_schedule = app.config["CELERY_BEAT_SCHEDULER"]
    celery.conf.timezone = "UTC"
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    # noinspection PyPropertyAccess
    # This works, regardless of what PyCharm tells you.
    celery.Task = ContextTask
    return celery
