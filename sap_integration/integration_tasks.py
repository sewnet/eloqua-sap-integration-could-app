from collections import namedtuple

task = namedtuple("Task", ("name", "value"))
frequency = namedtuple("Task", ("name", "value"))
inbound_tasks = (
    task("Create leads", "create_leads"),
    task("Update leads", "update_leads")
)

outbound_tasks = (
    task("Get Accounts", "Get_Accounts"),
    task("Get Contacts", "Get_Contacts"),
    task("Get International Leads", "Get_International_Leads"),
    task("Get Employees to Contact", "Get_Employees_to_Contact"),
    task("Get Finnish Leads", "Get_Finnish_Leads")
)

frequencies = (
    frequency("Daily", "Daily"),
    frequency("Hourly", "Hourly"),
    frequency("Weekly ", "Weekly")
)
